import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentSecurePage } from './payment-secure';

@NgModule({
  declarations: [
    PaymentSecurePage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentSecurePage),
  ],
})
export class PaymentSecurePageModule {}
