import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiveSingleDevice } from './live-single-device';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';

@NgModule({
  declarations: [
    LiveSingleDevice,
  ],
  imports: [
    IonicPageModule.forChild(LiveSingleDevice),
    IonBottomDrawerModule
  ],
})
export class LiveSingleDeviceModule {}